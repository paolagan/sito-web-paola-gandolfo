
// smooth scroll
$(document).ready(function(){
    $(".navbar .nav-link").on('click', function(event) {

        if (this.hash !== "") {

            event.preventDefault();

            var hash = this.hash;

            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 700, function(){
                window.location.hash = hash;
            });
        }
    });


});

// protfolio filters
$(window).on("load", function() {
    var t = $(".portfolio-container");
    t.isotope({
        filter: ".web",
        animationOptions: {
            duration: 750,
            easing: "linear",
            queue: !1
        }
    }), $(".filters a").click(function() {
        $(".filters .active").removeClass("active"), $(this).addClass("active");
        var i = $(this).attr("data-filter");
        return t.isotope({
            filter: i,
            animationOptions: {
                duration: 750,
                easing: "linear",
                queue: !1
            }
        }), !1
    });
});


$( window ).resize(function() {

  if ($( window ).width() < $( window ).height()) {
    $( "a[href='https://www.recod3.com/demos/next_release/ismartskin/parallax4obj/doctor_who/desktop/index.html']" ).attr('href', 'https://www.recod3.com/demos/next_release/ismartskin/parallax4obj/doctor_who/mobile/index.html');
  }else {
    $( "a[href='https://www.recod3.com/demos/next_release/ismartskin/parallax4obj/doctor_who/mobile/index.html']" ).attr('href', 'https://www.recod3.com/demos/next_release/ismartskin/parallax4obj/doctor_who/desktop/index.html');

  }
  console.log($( window ).width());
});
